package lab;

import lab.exception.InvalidStartingCoordinatesException;
import lab.exception.NoEscapeException;
import lab.model.Point;
import lab.util.MatrixUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Tremaux style algorithm using depth first search.
 */
public class LabEscape {

    private static final char WALL = 'O';
    private static final char FREE = ' ';
    static final char PATH = '•';
    static final char TRACE = 'X'; // only for debugging, denotes where the algorithm been walking
    private static final Logger LOGGER = LoggerFactory.getLogger(LabEscape.class);

    /**
     * @param labyrinth A labyrinth drawn on a matrix of characters. It's at least 4x4, can be a rectangle or a square.
     *                  Walkable areas are represented with a space character, walls are represented with a big O character.
     *                  The escape point is always on the border (see README)
     * @param startX    Starting row number for the escape. 0 based.
     * @param startY    Starting column number for the escape. 0 based.
     * @return A char matrix with the same labyrinth and a path drawn from the starting point to the escape
     * @throws NoEscapeException when no path exists to the outside, from the selected starting point
     */
    public static char[][] drawPathForEscape(char[][] labyrinth, int startX, int startY) throws NoEscapeException {

        Map<Point, Integer> pointsToVisitsNumberMap = new HashMap<>();
        Stack<Point> pointsVisited = new Stack<>();

        try {
            if (labyrinth[startX][startY] != FREE) {
                throw new InvalidStartingCoordinatesException("Invalid starting point: x = " + startX + ", y = " + startX + ". Make sure its on free area.");
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
            throw new InvalidStartingCoordinatesException("Invalid starting point: x = " + startX + ", y =" + startX + ". Make sure its within the labyrinth coordinates.");
        }

        Point startingPoint = new Point(startX, startY);
        pointsVisited.push(startingPoint);
        pointsToVisitsNumberMap.put(startingPoint, 1);

        Point currentPoint = startingPoint;
        Optional<Point> nextPointToGoTo;

        while (!pointsVisited.empty() && !isEscapePoint(labyrinth, currentPoint)) {

            LOGGER.debug("Current point: {}", currentPoint);

            nextPointToGoTo = getNextAvailable(currentPoint, pointsToVisitsNumberMap, labyrinth);
            LOGGER.debug("Next point to go to: {}", nextPointToGoTo);

            if (nextPointToGoTo.isPresent()) {
                incrementNumberOfTimesVisited(nextPointToGoTo.get(), pointsToVisitsNumberMap); // mark first time
                pointsVisited.push(currentPoint);
                LOGGER.debug("Pushed current point: {}", nextPointToGoTo);
                currentPoint = nextPointToGoTo.get();
            } else {
                incrementNumberOfTimesVisited(currentPoint, pointsToVisitsNumberMap);
                currentPoint = pointsVisited.pop();
            }
        }

        if (LOGGER.isDebugEnabled()) {
            pointsToVisitsNumberMap.forEach((point, timesVisited) -> LOGGER.debug("Point: {}; Number of times visited: {}", point, timesVisited));
        }

        if (pointsToVisitsNumberMap.get(startingPoint).equals(2)) {
            throw new NoEscapeException();
        }

        if (LOGGER.isDebugEnabled()) {
            pointsToVisitsNumberMap.forEach((key, value) -> {
                if (value == 1)
                    labyrinth[key.getX()][key.getY()] = PATH;
                else
                    labyrinth[key.getX()][key.getY()] = TRACE;
            });
            LOGGER.debug("\n" + MatrixUtils.toString(labyrinth));
        } else {
            pointsToVisitsNumberMap.entrySet().stream().filter(pointVisitedSingleTime -> pointVisitedSingleTime.getValue() == 1).forEach(
                    pointVisitedSingleTime -> labyrinth[pointVisitedSingleTime.getKey().getX()][pointVisitedSingleTime.getKey().getY()] = PATH);
        }
        return labyrinth;
    }

    static boolean isEscapePoint(char[][] labyrinth, Point currentPoint) {
        return pointIsValid(labyrinth, currentPoint) && pointLiesOnAnyBorder(labyrinth, currentPoint);
    }

    static Optional<Point> getNextAvailable(Point point, Map<Point, Integer> stateMap, char[][] labyrinth) {
        List<Point> candidates = new ArrayList<Point>() {{
            add(new Point(point.getX() - 1, point.getY()));
            add(new Point(point.getX() + 1, point.getY()));
            add(new Point(point.getX(), point.getY() + 1));
            add(new Point(point.getX(), point.getY() - 1));
        }};

        List<Point> validCandidates = getValidCandidates(stateMap, labyrinth, candidates);

        Point chosenPointToReturn = null;

        if (validCandidates.size() > 1) {
            chosenPointToReturn = validCandidates.get(getRandomIndex(validCandidates));
        } else if (validCandidates.size() == 1) {
            chosenPointToReturn = validCandidates.get(0);
        }

        return Optional.ofNullable(chosenPointToReturn);
    }

    protected static List<Point> getValidCandidates(Map<Point, Integer> stateMap, char[][] labyrinth, List<Point> candidates) {
        return candidates.stream()
                .filter(pointToCheckForWalls -> labyrinth[pointToCheckForWalls.getX()][pointToCheckForWalls.getY()] != WALL)
                // check we didn't step there before
                .filter(pointToCheckForPastVisit -> !stateMap.containsKey(pointToCheckForPastVisit))
                .collect(Collectors.toList());
    }

    private static int getRandomIndex(List<Point> validCandidates) {
        return new Random().nextInt(validCandidates.size());
    }

    private static void incrementNumberOfTimesVisited(Point point, Map<Point, Integer> pointNumberOfTimesVisited) {
        Integer result = pointNumberOfTimesVisited.putIfAbsent(point, 1);
        if (result != null) {
            pointNumberOfTimesVisited.put(point, ++result);
        }
    }

    private static boolean pointLiesOnAnyBorder(char[][] labyrinth, Point currentPoint) {
        return currentPoint.getX() == labyrinth.length - 1 || currentPoint.getX() == 0 ||
                currentPoint.getY() == labyrinth[currentPoint.getX()].length - 1 || currentPoint.getY() == 0;
    }

    private static boolean pointIsValid(char[][] labyrinth, Point currentPoint) {
        char mazeChar;
        try {
            mazeChar = labyrinth[currentPoint.getX()][currentPoint.getY()];
        } catch (ArrayIndexOutOfBoundsException indexOutOfBoundsException) {
            LOGGER.error("Attempted to check coordinates lying outside the maze!!! This should NOT happen.");
            indexOutOfBoundsException.printStackTrace();
            return false;
        }

        if (mazeChar != FREE) {
            LOGGER.warn("Attempted to check the WALL or other solid obstacle for escape point!? This should NOT happen.");
            return false;
        }
        return true;
    }
}
