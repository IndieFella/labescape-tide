package lab.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Queue;

public class MatrixUtils {

    private static Logger LOGGER = LoggerFactory.getLogger(MatrixUtils.class);

    public static char[][] fileToMatrix(InputStream inputStream) throws IOException {
        Queue<char[]> queue = new ArrayDeque<>();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new BufferedInputStream(inputStream), "UTF-8"))) {
            bufferedReader.lines().forEach(line -> queue.add(line.toCharArray()));
        }

        char[][] mazeMatrix = new char[queue.size()][];

        for (int i = 0; i < mazeMatrix.length; i++) {
            mazeMatrix[i] = queue.poll();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Read maze:\n" + toString(mazeMatrix));
        }
        return mazeMatrix;
    }

    public static String toString(char[][] mazeMatrix) {
        StringBuilder sb = new StringBuilder();
        for (char[] elementsOnAxis : mazeMatrix) {
            for (char charOnAxis : elementsOnAxis) {
                sb.append(charOnAxis);
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Verifies if all cells of matrixOne match matrixTwo.
     * Tells an index at which char arrays not equal. First invalid char returns immediately.
     *
     * @param matrixOne
     * @param matrixTwo
     * @return
     */
    public static boolean firstMatrixEqualsToSecond(char[][] matrixOne, char[][] matrixTwo) {
        if (matrixOne.length != matrixTwo.length) {
            LOGGER.info("Matrix one length is not equal to matrix two length");
            return false;
        }
        for (int i = 0; i < matrixOne.length; i++) {
            if (matrixOne[i].length != matrixTwo[i].length) {
                LOGGER.info("Nested array of matrix one length is not equal to nested array of matrix two length at X: {}", i);
                return false;
            }
            for (int j = 0; j < matrixOne[i].length; j++) {
                if (matrixOne[i][j] != matrixTwo[i][j]) {
                    LOGGER.info("Char of matrix one is not equal to char of matrix two at X: {} and Y: {}", i, j);
                    return false;
                }
            }
        }
        return true;
    }


}
