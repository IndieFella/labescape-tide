package lab.controller;

import lab.LabEscape;
import lab.exception.InvalidStartingCoordinatesException;
import lab.exception.NoEscapeException;
import lab.util.MatrixUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("api")
public class RestAPI {
    private static final String RETURN_APPLICATION_OCTET_STREAM = "application/octet-stream";
    private static final String RETURN_SOLUTION_FILENAME = "mazeSolution";

    private static final String NO_ESCAPE_WAS_FOUND_FOR_THIS_MAZE_ERROR = "NO_ESCAPE_WAS_FOUND_FOR_THIS_MAZE";
    private static final String INVALID_STARTING_POINT_ERROR = "INVALID_STARTING_POINT";

    @PostMapping("/maze/x/{x}/y/{y}")
    public ResponseEntity uploadMaze(@RequestParam("file") MultipartFile file, @PathVariable int x, @PathVariable int y) throws IOException {
        char[][] readMaze = MatrixUtils.fileToMatrix(file.getInputStream());
        char[][] solvedMaze;
        try {
            solvedMaze = LabEscape.drawPathForEscape(readMaze, x, y);
        } catch (NoEscapeException noEscapeException) {
            return ResponseEntity.badRequest().body("\"" + NO_ESCAPE_WAS_FOUND_FOR_THIS_MAZE_ERROR + "\"");
        } catch (InvalidStartingCoordinatesException invalidStartingCoordinatesException) {
            invalidStartingCoordinatesException.printStackTrace();
            return ResponseEntity.badRequest().body("\"" + INVALID_STARTING_POINT_ERROR + "\"");
        }

        byte[] solvedMazeBytes = MatrixUtils.toString(solvedMaze).getBytes();

        return ResponseEntity.ok()
                .contentLength(solvedMazeBytes.length)
                .header("content-disposition", "attachment; filename=" + RETURN_SOLUTION_FILENAME)
                .contentType(MediaType.parseMediaType(RETURN_APPLICATION_OCTET_STREAM))
                .body(new ByteArrayResource(solvedMazeBytes));
    }
}
