package lab.exception;

public class InvalidStartingCoordinatesException extends RuntimeException {
    public InvalidStartingCoordinatesException(String message) {
        super(message);
    }
}
