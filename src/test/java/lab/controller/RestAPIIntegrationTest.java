package lab.controller;

import lab.LabEscapeTest;
import lab.util.MatrixUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"logging.level.lab=info"}) // need this as we don't want solution to include trace paths.
public class RestAPIIntegrationTest {
    private static final String HOME = "http://127.0.0.1:";
    @LocalServerPort
    private int LOCAL_SERVER_PORT;
    private static final String SIMPLE_MAZE_EXPECTED_SOLUTION_JUST_PATH_FILE = "simpleMazeExpectedSolutionJustPath";

    private RestTemplate restTemplate;

    @Before
    public void setUp() {
        restTemplate = new RestTemplate();
    }

    @Test
    public void uploadAndSolveMazeViaREST() throws Exception {
        final String startingX = "1";
        final String startingY = "8";

        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", new ClassPathResource(LabEscapeTest.SIMPLE_MAZE_FILE));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<>(map, headers);
        ResponseEntity<Resource> response = restTemplate.exchange(HOME + LOCAL_SERVER_PORT + "/api/maze/x/" + startingX + "/y/" + startingY, HttpMethod.POST, httpEntity, Resource.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(MatrixUtils.firstMatrixEqualsToSecond(
                MatrixUtils.fileToMatrix(new ClassPathResource(SIMPLE_MAZE_EXPECTED_SOLUTION_JUST_PATH_FILE).getInputStream()),
                MatrixUtils.fileToMatrix(response.getBody().getInputStream())));
    }

    @Test(expected = HttpClientErrorException.class)
    public void failToSolveAsInvalidStartingPoint() throws Exception {
        final String startingX = "999";
        final String startingY = "999";

        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("file", new ClassPathResource(LabEscapeTest.SIMPLE_MAZE_FILE));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<LinkedMultiValueMap<String, Object>> httpEntity = new HttpEntity<>(map, headers);
        restTemplate.exchange(HOME + LOCAL_SERVER_PORT + "/api/maze/x/" + startingX + "/y/" + startingY, HttpMethod.POST, httpEntity, Resource.class);
    }
}