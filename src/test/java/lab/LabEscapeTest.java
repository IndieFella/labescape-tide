package lab;

import lab.exception.NoEscapeException;
import lab.model.Point;
import lab.util.MatrixUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(LabEscape.class)
public class LabEscapeTest {
    private static final String ESCAPE_VERIFICATION_MAZE_FILE = "escapeVerificationMaze";
    private static final String COMPLEX_MAZE_FILE = "SUPERMAZEMADNESS";
    private static final String NO_ESCAPE_MAZE_FILE = "noEscapeMaze";
    private static final String SIMPLE_MAZE_EXPECTED_SOLUTION_FILE = "simpleMazeExpectedSolution";
    public static final String SIMPLE_MAZE_FILE = "simpleMaze";

    private Resource resource;

    /**
     * We want to predictably verify that the algorithm can recover after reaching dead end, to do that, we need to mock
     * random part of the algorithm. Which is nicely overcomplicated with static initial condition. We can't override them,
     * we can't verify specific method invocations easily as PowerMockito verifyStatic() is called on class level.
     * <p>
     * So I decided to partially mock the class static method responsible for giving next valid candidates. Since I give only one candidate, random pick is not
     * triggered, so I can control where maze goes which is handy - allowing predictable testing! Finally I created a file
     * with the path I want to see and dead end I want for algorithm to visit. After visiting dead end I make algorithm to operate as usual - no mocks anymore,
     * so it has to recover from dead end and find escape. MatrixUtils checks final result with correct file on cell by cell basis.
     **/
    @Test
    public void verifyDrawPathForEscapeWithSimpleMazePredictably() throws Exception {
        resource = new ClassPathResource(SIMPLE_MAZE_FILE);
        assertTrue(resource.exists());
        char[][] simpleLabyrinth = MatrixUtils.fileToMatrix(resource.getInputStream());
        PowerMockito.spy(LabEscape.class);

        when(LabEscape.getValidCandidates(Mockito.any(), Mockito.eq(simpleLabyrinth), Mockito.anyListOf(Point.class)))
                .thenReturn(new ArrayList<Point>() {{
                    add(new Point(2, 8));
                }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(2, 7));
        }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(2, 6));
        }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(2, 5));
        }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(1, 5));
        }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(1, 4));
        }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(1, 3));
        }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(2, 3));
        }}).thenReturn(new ArrayList<Point>() {{
            add(new Point(3, 3));
        }}).thenCallRealMethod();

        final int startingX = 1;
        final int startingY = 8;
        char[][] labyrinthResult = LabEscape.drawPathForEscape(simpleLabyrinth, startingX, startingY);

        assertTrue(labyrinthResult[3][3] == LabEscape.TRACE); // check that we visited dead end
        assertTrue(labyrinthResult[1][0] == LabEscape.PATH); // check we found an escape, path is never drawn, if it there is no escape.

        // check solution is matching to our expected solution
        assertTrue(MatrixUtils.firstMatrixEqualsToSecond(MatrixUtils.fileToMatrix(new ClassPathResource(SIMPLE_MAZE_EXPECTED_SOLUTION_FILE).getInputStream()), labyrinthResult));
    }

    @Test
    public void verifyGetValidCandidates() throws IOException {
        resource = new ClassPathResource(SIMPLE_MAZE_FILE);
        char[][] labyrinth = MatrixUtils.fileToMatrix(resource.getInputStream());

        final int startingX = 2;
        final int startingY = 8;

        // surrounded by walls and one visited point, should get point to the left
        Point point = new Point(startingX, startingY);
        Point upperPoint = new Point(startingX - 1, startingY);

        Map<Point, Integer> pointsVisitsNumber = new HashMap<>();
        pointsVisitsNumber.put(upperPoint, 1);

        Optional<Point> nextPoint = LabEscape.getNextAvailable(point, pointsVisitsNumber, labyrinth);

        assertTrue(nextPoint.isPresent());
        Point pointExpected = new Point(startingX, startingY-1);
        assertEquals(pointExpected, nextPoint.get());

    }

    @Test
    public void verifyEscapePoint() throws IOException {
        resource = new ClassPathResource(ESCAPE_VERIFICATION_MAZE_FILE);
        assertTrue(resource.exists());

        // clockwise valid escape points
        char[][] labyrinth = MatrixUtils.fileToMatrix(resource.getInputStream());
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(3, 0)));
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(2, 0)));
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(0, 4)));
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(0, 8)));
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(0, 9)));
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(2, 9)));
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(3, 9)));
        assertTrue(LabEscape.isEscapePoint(labyrinth, new Point(3, 6)));

        assertFalse(LabEscape.isEscapePoint(labyrinth, new Point(999, 999)));
        assertFalse(LabEscape.isEscapePoint(labyrinth, new Point(2, 2)));
        assertFalse(LabEscape.isEscapePoint(labyrinth, new Point(1, 0)));
    }

    @Test(expected = NoEscapeException.class)
    public void verifyNoEscapeIsThrown() throws IOException, NoEscapeException {
        resource = new ClassPathResource(NO_ESCAPE_MAZE_FILE);
        assertTrue(resource.exists());

        char[][] labyrinth = MatrixUtils.fileToMatrix(resource.getInputStream());
        LabEscape.drawPathForEscape(labyrinth, 1, 6);
    }

    /**
     * This is DEMO-ONLY-TEST to show the test behavior, paths can be different each time
     */
    @Test
    public void solveSuperMazeMadness() throws IOException, NoEscapeException {
        resource = new ClassPathResource(COMPLEX_MAZE_FILE);
        assertTrue(resource.exists());
        char[][] complexLabyrinth = MatrixUtils.fileToMatrix(resource.getInputStream());
        LabEscape.drawPathForEscape(complexLabyrinth, 6, 13);
    }


}