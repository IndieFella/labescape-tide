If you are cloning please use HTTPS link in the right corner otherwise navigate to 'Downloads' within 'Navigation' panel on the left.

# Production Run #

1. Locate: **/src/main/resources/application.yml**
2. Change *server:port:8080* if you are running something on 8080, or make the port available.
3. **~/Develop/Projects/tideaccount-labescape $ ./gradlew clean build** (or BAT version for DOS)
4. **java -jar build/libs/maze-cracker-1.0-SNAPSHOT.jar** OR **./gradlew bootRun** as a goal, if you are using IntelliJ then you can simply open **build.gradle** and IJ should recognise the main starter class along with Spring Boot.

Please contact me if there are issues.

**WARNING:** Please do not save maze files with IntelliJ, use text editor , I haven't figured this out yet but IntelliJ strips empty spaces at the line ends, which breaks tests for mazes having escapes at the right border or random shape mazes compensated by white spaces. **All tests should be green!
**
# Features #

* Tremaux based algorithm to find an exit from a maze
* Uses depth first traversal
* REST API allows to upload maze files and receive solutions, index.html will be served on root request to the server which uses REST API and allows form file upload.
 * Change *logging:level:lab:info* to *logging:level:lab:debug* in application.yml to see debug logs and **trace information('X')**(where algorithm has been i.e. which dead ends it visited).

# About #

Simple maze algorithm, which should find escape in any situation where there is an escape. It uses depth first search so tries to engage chosen path until all options are exhausted where it will turn back. The algorithm is not shortest path algorithm, so you may observe it going around a loop instead of selecting shorter path next to it. Since this is my first ever labyrinth I am super happy with the robustness I managed to achieve.

**References used:**

[Trémaux's algorithm](https://en.wikipedia.org/wiki/Maze_solving_algorithm)

[Depth First Traversal](https://www.tutorialspoint.com/data_structures_algorithms/depth_first_traversal.htm)

## Details ##

**Point = coordinate on a maze, represented by x and y
**

Typical cycle involves dephfirst traversal where every new point we step on is marked as visited once. When hitting dead end the algorithm goes back on points stepped before. On every step back algorithm checks if there are other unvisited points from previously visited ones. If there are, it goes to new unexplored branches, if there aren't, the algorithm marks the point twice. Points marked twice mean dead end and will never be considered as part of the solution path. All points marked exactly once mean the exit path. Starting point marked twice means there is no escape from this maze. Starting point will be marked twice when all other points were visited twice and algorithm picks the starting point as last known point(opportunity) from which it expects to have new pathways, but since there are none it is marked second time.

I used additional two datastructures, Map remembers how many times we stepped on particular point. This is to figure out what is the final solution path or if there is solution at all.
Stack remembers all the previous points we step on when we exhaust all new pathways from a current point. This allows us to return back and go to another unexplored direction.

## Testing Details ##

Initial condition on static member posed an interesting challenge. I had to resort to PowerMockito to spy on the class to mock the core method deciding on where to go, without it predictable testing would be impossible.

My testing philosophy was fairly straightforward - don't make people think or reconstruct in their head what the test maze scenarios look like -> simply load file mazes which developers can open and figure out what is that I am testing. Further improvement could be to additionally include x and y axis coordinates within the file.